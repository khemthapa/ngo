<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/front.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<title>The Wellbeing Foundation - Home</title>
<script type="text/javascript" src="js/slideshow.js"></script>
<script type="text/javascript" src="js/facebookLike.js"></script>
</head>

<body>

  <?php

  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Select.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/db/DatabaseConnection.php';

  $dbConnect = DatabaseConnection::getDbInstance();

  $resSelect = new Select();
  $resData = $resSelect->selectRec("tbl_page", array("pageDescription"), "WHERE pageTitle='home'");
  $resMissionData = $resSelect->selectRec("tbl_page", array("pageDescription"), "WHERE pageTitle='ourMission'");

  ?>

<div class="container">
  <?php include 'header.php'; ?>
  <div class="sidebar1" style="clear:both">

     <?php include 'sidebar.php' ?>
   
  </div>  <!-- end .sidebar1 -->
  
  <div class="content">
  
  <div id="slideshow">
    <img src="images/gambia 9.jpg" alt="Home" width="100%" height="260" class="active"/>
    <img src="images/Gambia Chloe 1.jpg" alt="Home" width="100%" height="260"/>
    <img src="images/Gambia Chloe 17.jpg" alt="Home" width="100%" height="260"/>
    <img src="images/Gambia Dan 54.jpg" alt="Home" width="100%" height="260"/>
    </div>
 
    <h3> <font face="Times New Roman, Times, serif"; color="Black" > Middlesex University </font></h3><h1><font face="Times New Roman, Times, serif"; color="Black" >Well Being Foundation </font></h1>
    <p> 
        <?php 

        if(mysql_num_rows($resData) == 1) {
           $rowData = mysql_fetch_object($resData);
           $homeDescription = $rowData->pageDescription;
        }
         
        else
          $homeDescription = "";

          echo $homeDescription;
        ?>
    </p>

    <h2>Our Mission</h2>
    <p>
       <?php 

        if(mysql_num_rows($resData) == 1) {
           $rowMissionData = mysql_fetch_object($resMissionData);
           $missionDescription = $rowMissionData->pageDescription;
        }
         
        else
          $missionDescription = "";

          echo $missionDescription;
        ?>
    </p>
    

 </div> <!-- end of content -->

</div>  <!-- end .container -->
    <div style="clear:both;">

  <?php include 'footer.php'; ?>
 </div>
</body>
</html>