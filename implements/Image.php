<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Select.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/db/DatabaseConnection.php';

class Image
{
	private $imageTitle;
	private $images;

	public function get_event_images($eventId, $numbersOfImages)
	{

	  $dbConnect = DatabaseConnection::getDbInstance();
	  $this->images = array();
	  $resSelect = new Select();
	  if ($numbersOfImages == "all")
	  	$resData = $resSelect->selectRec("tbl_image", "*", "WHERE eventId = '$eventId'");
	  else
	  	$resData = $resSelect->selectRec("tbl_image", "*", "WHERE eventId = '$eventId' LIMIT $numbersOfImages");

	  if(mysql_num_rows($resData) > 0)  
	  {
	  	while($rowImage = mysql_fetch_object($resData)):
	  		$dataValues = array("id" => "$rowImage->id", 
	                        "imageTitle"=> "$rowImage->imageTitle", 
	                        "imageName" => "$rowImage->imageName");
	  		array_push($this->images, $dataValues);
	  	endwhile;

	  	return $this->images;
	  }
	  else   return false;

	}

}
