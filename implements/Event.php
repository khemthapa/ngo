<?php 

class Event
{
	private $date;
 	private $today;
 	private $upcomingEvents = array();
 	private $pastEvents = array();

 	private $eventId;

 	function __construct($resEvents)
 	{

 		$this->date = date('d-m-Y');
 		$this->today = new DateTime($this->date);

 		while ($rowEvent = mysql_fetch_object($resEvents)):

	     $eventDate = $rowEvent->eventDate;
	     $eventDateTime = new DateTime($eventDate);

	     $interval = $this->today->diff($eventDateTime);
	     $dayGap = $interval->format('%R%a'); //in days

	     $dataValues = array("id" => "$rowEvent->id", 
	                        "eventTitle"=> "$rowEvent->eventTitle", 
	                        "eventDate"=> "$rowEvent->eventDate",
	                        "eventDescription" => "$rowEvent->eventDescription");

	     if($dayGap > 0)  array_push($this->upcomingEvents, $dataValues);
	     else array_push($this->pastEvents, $dataValues);

	     endwhile;

 	}

 	public function get_upcoming_events()
 	{
 		return $this->upcomingEvents;
 	}

 	public function get_past_events()
 	{
 		return $this->pastEvents;
 	}
}