<?php

include $_SERVER['DOCUMENT_ROOT'].'/protocols/ReadRecs.php';

class Select implements ReadRecs 
{
	
	private $sql;
	
	private $resSql;
	
	private $numRows;
	
	public function selectRecs($tblName, $attrs) {
		
		if($attrs == "*") {
		
			 $this->sql = "SELECT * FROM $tblName ORDER BY id DESC";
			
		}
		else {
		
			$attrsArray = " ";
			
			foreach($attrs as $key=>$value) {
			
				$attrsArray = $attrsArray.", ".$value;
				
			}
			
			$attrsArray = substr($attrsArray,2);
			$this->sql = "SELECT $attrsArray FROM $tblName ORDER BY id DESC";
		}

		return $this->resSql = mysql_query($this->sql);
		
	}
	
	public function selectRec($tblName, $attrs, $where) {
		
		if($attrs == "*") {
		
			$this->sql = "SELECT * FROM $tblName $where";
			
		}
		else {
		
			$attrsArray = ",";
			
			foreach($attrs as $key=>$value) {
			
				$attrsArray = $value.$attrsArray;
				
			}
			
			$attrsSelected = substr($attrsArray,0,-1);
			$this->sql = "SELECT $attrsSelected  FROM $tblName $where";
		}

		return $this->resSql = mysql_query($this->sql);
		
	}
}