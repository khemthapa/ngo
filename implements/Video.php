<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Select.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/db/DatabaseConnection.php';

class Video
{
	private $videoTitle;
	private $videos;

	public function get_event_videos($eventId, $numberOfVideos)
	{

	  $dbConnect = DatabaseConnection::getDbInstance();
	  $this->videos = array();
	  $resSelect = new Select();
	  if ($numberOfVideos == "all")
	  	$resData = $resSelect->selectRec("tbl_video", "*", "WHERE eventId = '$eventId'");
	  else
	  	$resData = $resSelect->selectRec("tbl_video", "*", "WHERE eventId = '$eventId' LIMIT $numberOfVideos");

	  if(mysql_num_rows($resData) > 0)  
	  {
	  	while($rowVideo = mysql_fetch_object($resData)):
	  		$dataValues = array("id" => "$rowVideo->id", 
	                        "videoTitle"=> "$rowVideo->videoTitle", 
	                        "videoLink" => "$rowVideo->videoLink");
	  		array_push($this->videos, $dataValues);
	  	endwhile;

	  	return $this->videos;
	  }
	  else   return false;

	}
}