<?php

include_once("config.php");
//use config;

class DatabaseConnection 
{
	private static $dbInstance = null;
	private $connection;
	
	private function __construct() {

			$this->connection = mysql_pconnect(config\databaseServer, config\dbUser, config\dbPassword) 
			or die(mysql_error()); 
			
			mysql_select_db(config\dbName) or die(mysql_error());

	}
	
	public static function getDbInstance() {
	
		if(self::$dbInstance == null) 
		    self::$dbInstance = new DatabaseConnection();

		return self::$dbInstance;
			
	}
	
}