<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/front.css">
<script type="text/javascript" src="js/slideshow.js"></script>
<script type="text/javascript" src="js/facebookLike.js"></script>
<title>The Wellbeing Foundation - Courses</title>

</head>

<body>

  <?php

  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Select.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/db/DatabaseConnection.php';

  $dbConnect = DatabaseConnection::getDbInstance();

  $resSelect = new Select();
  $resData = $resSelect->selectRecs("tbl_course", array("courseTitle","courseDescription"));

  ?>

<div class="container">

	<?php include 'header.php'; ?>

  <div class="sidebar1">

     <?php include 'sidebar.php' ?>

  </div>     <!-- end .sidebar1 -->
   
  <div class="content">
  	 <div id="slideshow">
	  <img src="images/gambia 9.jpg" alt="Home" width="100%" height="260" class="active"/>
	  <img src="images/Gambia Chloe 1.jpg" alt="Home" width="100%" height="260"/>
	  <img src="images/Gambia Chloe 17.jpg" alt="Home" width="100%" height="260"/>
	  <img src="images/Gambia Dan 54.jpg" alt="Home" width="100%" height="260"/>
	  </div>
    <h2>Courses offered</h2>
    <hr />
    <p>

      <?php 

        if(mysql_num_rows($resData) > 0) {

           while($rowData = mysql_fetch_object($resData))
           {
              $courseTitle = $rowData->courseTitle;
              $courseDescription = $rowData->courseDescription;

              echo "<h4>$courseTitle</h4>";

              echo "<p>$courseDescription</p>";
           }
           
        }

        else {
            echo "<p> No Courses Available at the moment. </p>";
        }

        ?>

    </p>
    
   

    <!-- end .content --></div>
  </div> <!-- end .container -->

<div>
  <?php include 'footer.php'; ?> 
</div>
</body>
</html>