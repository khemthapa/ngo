<?php

interface ReadRecs
{
	 public function selectRec($tableName, $attributes, $where);
	 
	 public function selectRecs($tableName, $attributes);
	 
}