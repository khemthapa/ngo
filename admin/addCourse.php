<?php

include_once '../implements/Add.php';
$request = new RequestProtection();


if(isset($_POST['addButton']) && $request->is_valid())
{

	$courseTitle = input($_POST["courseTitle"]);
	
	$courseDescription = input($_POST["courseDescription"]);
	
	$dbConnect = DatabaseConnection::getDbInstance();
	
	$add = new Add();
	
	$resAdd = $add->addContents("tbl_course", array("courseTitle" => $courseTitle,
									"courseDescription" => $courseDescription,
									"addedDate" => date("Y-m-d H:i:s"),
					  ));
	if($resAdd) {
		header("Location: dashboard.php?page=manageCourses");
		Session::setSessionLogin("addedSuccessfully", "New Course has been added successfully.");
	}
}


?>

<div id="welcome_page">	
	<span class="content_header"> Add Course</span>
	
	<form id="addEventForm" method="post" name="addEventForm"  enctype="multipart/form-data" action="" onSubmit="return validateInput(this)">
	
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />

	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">
	
	<p id="err_courseTitle" name = "err_courseTitle" class="notifyError"></p> 
	<input  name="err_courseTitle_msg" id="err_courseTitle_msg"  type="hidden" value="Course Title is Missing!" />
	
	<p id="err_eventDate" name = "err_eventDate" class="notifyError"> </p> 
	<input  name="err_eventDate_msg" id="err_eventDate_msg"  type="hidden" value="Event Date is Missing!" />
	
	<p id="err_courseDescription" name = "err_courseDescription" class="notifyError"></p> 
	<input  name="err_courseDescription_msg" id="err_courseDescription_msg"  type="hidden" value="Course Description is Missing!" />
	
	<tr>
		<td height="45" width="0" >Course Title: *</td>
		
		<td height="45" width="0">
		
			<input id="courseTitle" class="form_text" type="text" name="courseTitle"  />
			
		</td>
		
	</tr>
	
	<tr>
		<td height="5" width="0" colspan="2"></td>
	</tr>	
	
	<tr>
		<td height="45" width="0" valign="top">Course Description: *</td>
		
		<td height="45" width="0" >
			<textarea name="courseDescription" id="courseDescription" rows="4" class="form_textarea" ></textarea>
		</td>
		
	</tr>

	
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		
		<input id="addButton" name="addButton" class="log_button" type="submit" value="Add" />
		<input id="addButton" name="addButton" type="hidden" value="addCourse" />
		
		</td>
	</tr>
	</table>
	
	</form>
</div>
