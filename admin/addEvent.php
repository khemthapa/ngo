<?php

include_once '../implements/Add.php';
$request = new RequestProtection();

if(isset($_POST['addButton']) && $request->is_valid())
{
	$eventTitle = input($_POST['eventTitle']);
	
	$eventDate = input($_POST['eventDate']);
	
	$eventDescription = input($_POST['eventDescription']);
	
	$eventStartTime = input($_POST['eventStartTime']);
	
	$eventEndTime = input($_POST['eventEndTime']);
	
	$dbConnect = DatabaseConnection::getDbInstance();
	
	$add = new Add();
	
	$resAdd = $add->addContents('tbl_event', array('eventTitle' => $eventTitle,
									'eventDescription' => $eventDescription,
									'eventDate' => $eventDate,
									'eventStartTime' => $eventStartTime,
									'eventEndTime' => $eventEndTime,
									'addedDate' => date('Y-m-d h:i:s')
					  ));
	if($resAdd) {
		header('Location: dashboard.php?page=manageEvents');
		Session::setSessionLogin('addedSuccessfully', 'New Event has been added successfully.');
	}
}


?>

<div id="welcome_page">	
	<span class="content_header"> Add an Event</span>
	
	<form id="addEventForm" method="post" name="addEventForm"  enctype="multipart/form-data" 
	onSubmit="return validateInput(this)">
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />
	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">
	
	<p id="err_eventTitle" name = "err_eventTitle" class="notifyError"></p> 
	<input  name="err_eventTitle_msg" id="err_eventTitle_msg"  type="hidden" value="Event Title is Missing!" />
	
	<p id="err_eventDate" name = "err_eventDate" class="notifyError"> </p> 
	<input  name="err_eventDate_msg" id="err_eventDate_msg"  type="hidden" value="Event Date is Missing!" />
	
	<p id="err_eventDescription" name = "err_eventDescription" class="notifyError"></p> 
	<input  name="err_eventDescription_msg" id="err_eventDescription_msg"  type="hidden" value="Event Description is Missing!" />
	
	<tr>
		<td height="45" width="0" >Event Title: *</td>
		
		<td height="45" width="0">
		
			<input id="eventTitle" class="form_text" type="text" name="eventTitle"  />
			
		</td>
		
	</tr>

	<tr>
		<td height="45" width="0" >Event Date: *</td>
		
		<td height="45" width="0">
		
			<input id="eventDate" class="date_text" type="text" name="eventDate" size="15" readonly />
			
		</td>
		
	</tr>
	
	<tr>
		<td height="45" width="0" >Start Time:</td>
		
		<td height="45" width="0">
		<select id="eventStartTime" name="eventStartTime" class="time_text" >
			<?php for($d=00; $d<24; $d++):
				if($d<10)  $startDate = '0'.$d;
				else $startDate = $d;
			?>
			<option value="<?php echo $startDate;?>"><?php echo $startDate.'00';?></option>
			<?php endfor;?>
		</select> HRS
		</td>
		
	</tr>
	<tr>
	<td height="45" width="0" >End Time:</td>
	<td height="45" width="0">
			<select id="eventEndTime" name="eventEndTime" class="time_text" >
				<?php for($d=00; $d<24; $d++):
					if($d<10)  $endDate = '0'.$d;
					else $endDate = $d;
				?>
				<option value="<?php echo $endDate;?>"><?php echo $endDate.'00';?></option>
				<?php endfor;?>
			</select> HRS
			</td>
	</tr>
	<tr>
		<td height="5" width="0" colspan="2"></td>
	</tr>	
	
	<tr>
		<td height="45" width="0" valign="top">Event Description: *</td>
		
		<td height="45" width="0" >
			<textarea name="eventDescription" id="eventDescription" rows="4" class="form_textarea" ></textarea>
		</td>
		
	</tr>

	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		
		<input id="addButton" name="addButton" class="log_button" type="submit" value="Add" />
		<input id="addButton" name="addButton" type="hidden" value="addEvent" />
		
		</td>
	</tr>
	</table>
	
	</form>
</div>
