<?php

include_once '../implements/Select.php';
include_once '../implements/Add.php';
$request = new RequestProtection();

$dbConnect = DatabaseConnection::getDbInstance();
$imgDir = '../well_images/';

$addImage = new Add();

$resSelect = new Select();
$resEventData = $resSelect->selectRecs("tbl_event", array('id', 'eventTitle'));

if(isset($_POST['addButton']) && $request->is_valid())
{
	$arrayImageTitle = $_POST['imageTitle'];
	$eventId = $_POST['eventId'];
	$isAdded = false;
	
	if(isset($_FILES['image']['name']))
	{
		foreach($_FILES['image']['name'] as $key=>$val)
		{
			if(!empty($val)) {
				
				$extension = getExtension($val);
				$randChars = getRandomKeys(12);
				$imageName = $randChars.$val;
				$imageUploaded = $imgDir.$imageName;
				$tmpName = $_FILES['image']['tmp_name'][$key];
				if($extension=="jpg" || $extension=="jpeg" || $extension=="png")
				{
					if(move_uploaded_file($tmpName, $imageUploaded))
					{
						$imageTitle = $arrayImageTitle[$key];
							
						$resAdd = $addImage->addContents('tbl_image', array('imageTitle' => $imageTitle,
														'imageName' => $imageName,
														'eventId' => $eventId,
														'addedDate' => date('Y-m-d h:i:s')
														));
						if($resAdd)
						{
							$isAdded= true;
						}
					}
				}
				
			}
		}
		if($isAdded) header('Location: dashboard.php?page=manageImages');
	}
	else
	{
		//echo "No Images";
	}
	
}

?>

<div id="welcome_page">	

	<span class="content_header"> Add Image(s)</span>
	
	<form id="addImagesForm" method="post" name="addImagesForm"  enctype="multipart/form-data" onSubmit="return validateInputImageForm(this)">
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />

	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">

	<p id="err_eventTitle" class="notifyError"></p> 

	<tr>
		<td height="45" width="0" >Event Title: *</td>
		
		<td height="45" width="0">

			 <select id = "eventId" class="form_text" name="eventId" style="padding:0px;">
               <option value = "-1">Select an Event</option>
			   <?php
			
			   	if(mysql_num_rows($resEventData) == 0)
				{ 
				?>
					<option value = "-1">No Events</option>
				<?php
				}
				else
				{ 
					while($rowEventData = mysql_fetch_object($resEventData)) {
				   ?>
					   <option value = "<?php echo $rowEventData->id?>">
					   <?php echo $rowEventData->eventTitle;?>
					   </option>
				   <?php
					}
				}
			   ?>
             </select>
			
		</td>
		
	</tr>
	
	<tr>
		<td height="45" width="0" >Image Title:</td>
		
		<td height="45" width="0">
		
			<input id="imageTitle" class="form_text" type="text" name="imageTitle[]"  />
			
		</td>
		
	</tr>

	<tr>
		<td height="5" width="0" colspan="2"></td>
	</tr>	
	
	<tr>
	
		<td height="45" width="0">Add Image(s): </td>
		
		<td height="45" width="0" >
			<input type="file" name="image[]" id="image" />
			<span><a href="javascript:addMore()" class="" >Add More</a> >> </span>
		</td>
		
	</tr>
	<tr>
	<td width="0"></td>
	<td width="0">
	<table id="addTable" width="100%"></table>
	</td>
	</tr>
	
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="addButton" name="addButton" class="log_button" type="submit" value="Add" />
		<input id="addButton" name="addButton" type="hidden" value="addImages" />
		</td>
	</tr>
	</table>
	
	</form>
</div>
