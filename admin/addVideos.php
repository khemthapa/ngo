<?php

include_once '../implements/Select.php';
include_once '../implements/Add.php';
$request = new RequestProtection();

$imgDir = '../well_images/';
$dbConnect = DatabaseConnection::getDbInstance();

$addVideo= new Add();

$resSelect = new Select();
$resEventData = $resSelect->selectRecs("tbl_event", array('id', 'eventTitle'));

if(isset($_POST['addButton']) && $request->is_valid())
{
	$arrayVideoTitle = $_POST['videoTitle'];
	$arrayVideoLink = $_POST['videoLink'];
	$eventId = $_POST['eventId'];
	$isAdded= false;

	foreach($arrayVideoLink as $key=>$val)
		{
			if(!empty($val)) {
				$videoTitle = $arrayVideoTitle[$key];
				$resAdd = $addVideo->addContents('tbl_video', array('videoTitle' => $videoTitle,
														'videoLink' => $val,
														'eventId' => $eventId,
														'addedDate' => date('Y-m-d h:i:s')
														));
				if($resAdd)
				{
					$isAdded= true;
				}
			}
		}
		
	if($isAdded) header('Location: dashboard.php?page=manageVideos');
}
?>

<div id="welcome_page">	

	<span class="content_header"> Add Video(s)</span>
	
	<form id="addImagesForm" method="post" name="addImagesForm"  enctype="multipart/form-data" onSubmit="return validateInputImageForm(this)">
	
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />

	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">

	<p id="err_eventTitle" class="notifyError"></p> 

	<tr>
		<td height="45" width="0" >Event Title: *</td>
		
		<td height="45" width="0">

			 <select id = "eventId" class="form_text" name="eventId" style="padding:0px;">
               <option value = "-1">Select an Event</option>
			   <?php
			
			   	if(mysql_num_rows($resEventData) == 0)
				{ 
				?>
					<option value = "-1">No Events</option>
				<?php
				}
				else
				{ 
					while($rowEventData = mysql_fetch_object($resEventData)) {
				   ?>
					   <option value = "<?php echo $rowEventData->id?>">
					   <?php echo $rowEventData->eventTitle;?>
					   </option>
				   <?php
					}
				}
			   ?>
             </select>
			
		</td>
		
	</tr>
	
	<tr>
		<td height="45" width="0" >Video Title:</td>
		
		<td height="45" width="0">
		
			<input id="videoTitle[]" class="form_text" type="text" name="videoTitle[]"  />
			
		</td>
		
	</tr>

	<tr>
		<td height="5" width="0" colspan="2"></td>
	</tr>	
	
	<tr>
	
		<td height="45" width="0">Embedded Video Link: </td>
		
		<td height="45" width="0" >
			<input id="videoLink[]" class="video_text" type="text" name="videoLink[]"  />
			<span><a href="javascript:addMoreVideos()" class="" >Add More</a> >> </span>
		</td>
		
	</tr>
	
	<tr>
	<td width="0"></td>
	<td width="0">
	<table id="addTable" width="100%"></table>
	</td>
	</tr>
	
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="addButton" name="addButton" class="log_button" type="submit" value="Add" />
		<input id="addButton" name="addButton" type="hidden" value="addVideos" />
		</td>
	</tr>
	</table>
	
	</form>
</div>
