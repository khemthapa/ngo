<?php

include_once("../implements/Select.php");
include_once("../implements/Update.php");
$request = new RequestProtection();

if(isset($_GET['id'])) 
	$pageId = $_GET['id'];
else
	header("Location: error403.php");

	
$dbConnect = DatabaseConnection::getDbInstance();

$resSelect = new Select();
$resData = $resSelect->selectRec("tbl_page", "*", "WHERE id='$pageId'");

if(mysql_num_rows($resData) == 1)
	$rowData = mysql_fetch_object($resData);
else
	header("Location: error403.php");
	
if(isset($_POST['editButton']) && $request->is_valid())
{
	$pageDescription = input($_POST["pageDescription"]);
	
	$editPage = new Update();
	$resData = $editPage->editContents("tbl_page", array("pageDescription" => $pageDescription), 
			   "WHERE id='$pageId'");
	if($resData) {
		
		Session::setSessionLogin("editedSuccessfully", $rowData->pageName." page has been edited successfully.");
		header("Location: dashboard.php?page=managePages");
		
	}
		
}

?>

<div id="welcome_page">	
	<span class="content_header"> Edit Page </span>
	
	<form id="editPageForm" method="post" name="editPageForm" onSubmit="return validateInput(this)">
	
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />

	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">
	
	<tr>
		<td height="45" width="0" >Page Title:</td>
		
		<td height="45" width="0">
		
			<input id="pageTitle" class="form_text" type="text" name="pageTitle" 
			value="<?php echo $rowData->pageName;?>" readonly="true"  />
			
		</td>
		
	</tr>
	
	<tr>
		<td height="45" width="0" valign="top">Page Description: *</td>
		
		<td height="45" width="0" >
		
			<textarea name="pageDescription" id="pageDescription" rows="4" 	class="form_textarea" ><?php echo $rowData->pageDescription;?></textarea>
			<br/><span id="err_pageDescription" class="missingError"></span> 
			
		</td>
		
	</tr>
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="editButton" name="editButton" class="log_button" type="submit" value="Edit" />
		<input id="editButton" name="editButton" type="hidden" value="editContent" />
		</td>
	</tr>
	</table>
	
	</form>
</div>
