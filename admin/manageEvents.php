<?php

include_once '../implements/Select.php';

$dbConnect = DatabaseConnection::getDbInstance();

$resSelect = new Select();
$resData = $resSelect->selectRecs("tbl_event", "*");
$count = 0;

?>

<div id="welcome_page">
  
	<span class="content_header" > 
		Manage Events | <a href="dashboard.php?page=addEvent" class="newAction">Add New Event</a> 
	</span>
		
	<table id="page_table" name="page_table" cellpadding="0" cellspacing="0" width="100%">
	<?php 
	
	if(isset($_SESSION['editedSuccessfully'])) 
	{
		echo "<p class='notifySuccess'>".$_SESSION['editedSuccessfully']."</p>";
		unset($_SESSION['editedSuccessfully']);
	}
	if(isset($_SESSION['addedSuccessfully'])) 
	{
		echo "<p class='notifySuccess'>".$_SESSION['addedSuccessfully']."</p>";
		unset($_SESSION['addedSuccessfully']);
	}
	if(isset($_SESSION['deleteSuccessFully'])) 
	{
		echo "<p class='notifySuccess'>".$_SESSION['deleteSuccessFully']."</p>";
		unset($_SESSION['deleteSuccessFully']);
	}
	
	if(mysql_num_rows($resData) == 0)
	{
		echo "<tr><td colspan='4' class='noRecsFound'>No Records Found</td></tr>";
	}
	else
	{
	?>
	<tr>
		<th height="30" width="8%" align="left">S.N.</th>
		<th height="30" width="0" align="left">Event Title</th>
		<th height="30" width="0" align="left">Event Date</th>
		<th height="30" width="0" align="left">Actions</th>
	</tr>
	<?php
		
			while($rowData = mysql_fetch_object($resData)):
			$count++;
	?>
			<tr>
				<td height="30" width="8%" align="left"> <?php echo $count.".";?></td>
				<td height="30" width="0" align="left"> <?php echo $rowData->eventTitle;?></td>
				<td height="30" width="0" align="left"> <?php echo $rowData->eventDate;?></td>
				<td height="30" width="0" align="left"> 
				<form id="delForm_<?php echo $rowData->id?>" name="delForm_<?php echo $rowData->id?>" action="delete.php" method="POST">
				<a href="dashboard.php?page=editEvent&id=<?php echo $rowData->id ?>" class="newAction">Edit</a> |
				<a href="javascript:document.getElementById('delForm_<?php echo $rowData->id?>').submit()" class="newAction">Delete</a>
				<input type="hidden" id="event_<?php echo $rowData->id?>" name="event_<?php echo $rowData->id?>" />
				</form>
				</td>
			</tr>
	<?php 
			endwhile;
			
		} //end of else
	?>

	</table>
						
</div>