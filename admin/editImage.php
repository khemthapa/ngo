<?php

include_once '../implements/Select.php';
include_once '../implements/Update.php';
$request = new RequestProtection();

if(isset($_GET['id'])) 
	$imageId = $_GET['id'];
else
	header("Location: error403.php");

$imgDir = '../well_images/';
	
$dbConnect = DatabaseConnection::getDbInstance();

$selEvent= new Select();
$resEventData = $selEvent->selectRecs("tbl_event", array('id', 'eventTitle'));

$resSelect = new Select();
$resData = $resSelect->selectRec("tbl_image", "*", "WHERE id='$imageId'");

if(mysql_num_rows($resData) == 1)
	$rowData = mysql_fetch_object($resData);
else
	header("Location: error403.php");
	
if(isset($_POST['editButton']) && $request->is_valid())
{
	$videoTitle = $_POST['imageTitle'];
	$eventId = $_POST['eventId'];
	
	$editVideo= new Update();
	
	if(isset($_FILES['image']['name']))
	{
		$newImage = $_FILES['image']['name'];
		if(!empty($newImages))
		{
			$extension = getExtension($newImage );
			$oldImageName = $rowData->imageName;
			$prevImageUploaded = $imgDir.$oldImageName;
			if(file_exists($prevImageUploaded)) unlink($precImageUploaded);
			$newImageUploaded = $imgDir.$newImage;
			$tmpName = $_FILES['image']['tmp_name'];
			if($extension=="jpg" || $extension=="jpeg" || $extension=="png")
			{
				move_uploaded_file($tmpName, $newImageUploaded);
				$resAdd = $editVideo->editContents('tbl_image', array('imageName' => $newImage										
													),
												"WHERE id='$imageId'");
			}
		}
		
	}
	
	$resAdd = $editVideo->editContents('tbl_image', array('imageTitle' => $videoTitle,
														'eventId' => $eventId
											),
										"WHERE id='$imageId'");
	if($resAdd)
	{	
		Session::setSessionLogin("editedSuccessfully", $rowData->imageTitle." has been edited successfully.");
		header('Location: dashboard.php?page=manageImages');
	}
		
}
?>

<div id="welcome_page">	

	<span class="content_header"> Edit Image</span>
	
	<form id="addImagesForm" method="post" name="addImagesForm"  enctype="multipart/form-data" onSubmit="return validateInputImageForm(this)">
	
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />
	
	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">

	<p id="err_eventTitle" class="notifyError"></p> 

	<tr>
		<td height="45" width="0" >Event Title: *</td>
		
		<td height="45" width="0">

			 <select id = "eventId" class="form_text" name="eventId" style="padding:0px;">
               <option value = "-1">Select an Event</option>
			   <?php
			
			   	if(mysql_num_rows($resEventData) == 0)
				{ 
				?>
					<option value = "-1">No Events</option>
				<?php
				}
				else
				{ 
					while($rowEventData = mysql_fetch_object($resEventData)) {
				   ?>
					   <option value = "<?php echo $rowEventData->id?>"
					   <?php echo ($rowData->eventId == $rowEventData->id)?"selected":"";?> >
					   <?php echo $rowEventData->eventTitle;?>
					   </option>
				   <?php
					}
				}
			   ?>
             </select>
			
		</td>
		
	</tr>
	
	<tr>
		<td height="45" width="0" >Image Title:</td>
		
		<td height="45" width="0">
		
			<input id="imageTitle" class="form_text" type="text" name="imageTitle" value="<?php echo $rowData->imageTitle?>"  />
			
		</td>
		
	</tr>

	<tr>
		<td height="5" width="0" colspan="2"></td>
	</tr>	
	
	<tr>
	
		<td height="45" width="0" valign="top">Image: </td>
		
		<td height="45" width="0" >
			<img src='<?php echo $imgDir.$rowData->imageName;?>' height='200' widht='200' class='imgBorder'/> 
			<br />
			<input type="file" name="image" id="image" />
		</td>
		
	</tr>
	
	
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="editButton" name="editButton" class="log_button" type="submit" value="Edit" />
		<input id="editButton" name="editButton" type="hidden" value="editImage" />
		</td>
	</tr>
	</table>
	
	</form>
</div>
