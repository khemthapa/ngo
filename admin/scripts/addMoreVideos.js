var count = 1;
function addMoreVideos()
{
	var inputTitle = document.createElement("input");
	inputTitle.setAttribute("name", "videoTitle[]");
	inputTitle.setAttribute("type", "text");
	inputTitle.setAttribute("class", "form_text");
	inputTitle.setAttribute("placeholder", "Video Title");
	
	var tdTitle = document.createElement("td");
	tdTitle.setAttribute("height", "45");
	tdTitle.appendChild(inputTitle);
	
	var trTitle = document.createElement("tr");
	trTitle.setAttribute("id", "trTitle_"+count);
	trTitle.appendChild(tdTitle);
	
	document.getElementById("addTable").appendChild(trTitle);
	
	var inputType = document.createElement("input");
	inputType.setAttribute("name", "videoLink[]");
	inputType.setAttribute("type", "text");
	inputType.setAttribute("class", "video_text");
	inputType.setAttribute("placeholder", "Embedded Video Link");
	
	var addLink = document.createElement("span");
	addLink.innerHTML = " <a href='javascript:addMoreVideos()'>Add More</a> >>"+ 
	" <a href='javascript:this.removeVid("+count+")'>Remove</a>";
	
	var td = document.createElement("td");
	td.setAttribute("height", "45");
	td.appendChild(inputType);
	td.appendChild(addLink);
	
	var tr = document.createElement("tr");
	tr.setAttribute("id", "tr_"+count);
	tr.appendChild(td);
	
	document.getElementById("addTable").appendChild(tr);
	document.getElementById("addTable").style.margin = "-3px";
	count++;
	
}

function removeVid(removeId)
{
	removeLink = "tr_"+removeId;
	removeTitle = "trTitle_"+removeId;
	document.getElementById(removeLink).style.display = "none";
	document.getElementById(removeTitle).style.display = "none";
}