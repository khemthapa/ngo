var count = 1;
function addMore()
{
	//starting for image title

	var imgTitleText = document.createElement("input");
	imgTitleText.setAttribute("name", "imageTitle[]");
	imgTitleText.setAttribute("type", "text");
	imgTitleText.setAttribute("class", "form_text");
	imgTitleText.setAttribute("placeholder", "Image Title");
	
	var tdTitle = document.createElement("td");
	tdTitle.setAttribute("height", "45");
	tdTitle.appendChild(imgTitleText);
		
	var trTitle = document.createElement("tr");
	trTitle.setAttribute("id", "tr_title_"+count);
	trTitle.appendChild(tdTitle);
	
		
	// for image file
	var inputType = document.createElement("input");
	inputType.setAttribute("name", "image[]");
	inputType.setAttribute("type", "file");
	
	var addLink = document.createElement("span");
	addLink.innerHTML = " <a href='javascript:addMore()'>Add More</a> >>"+ 
	" <a href='javascript:this.removeImg("+count+")'>Remove</a>";
	
	var td = document.createElement("td");
	td.setAttribute("height", "45");
	td.appendChild(inputType);
	td.appendChild(addLink);
		
	var tr = document.createElement("tr");
	tr.setAttribute("id", "tr_"+count);
	tr.appendChild(td);
	
	document.getElementById("addTable").appendChild(trTitle);
	document.getElementById("addTable").appendChild(tr);
	
	// end of image file
	
	document.getElementById("addTable").style.margin = "-3px";
	
	count++;
	
}

function removeImg(removeId)
{	
	var removeImage = "tr_"+removeId;
	var removeTitle = "tr_title_"+removeId;
	document.getElementById(removeImage).style.display = "none";
	document.getElementById(removeTitle).style.display = "none";
}