
function validateInputImageForm(form) 
{
	var returnValue = true;
	
	for(var i=0; i<form.length; i++) {
		
		if(form.elements[i].value.trim() == "-1") { 
		
			document.getElementById(form.elements[i].name).style.borderColor = "#F58C8D";
			document.getElementById(form.elements[i].name).style.borderWidth = "2px";
			
			if(document.getElementById("err_"+form.elements[i].name)) {
				document.getElementById("err_"+form.elements[i].name).innerHTML = "Please Select the event !";
			}
			
			return false;
			
		}
		else { 
			
			if(document.getElementById("err_"+form.elements[i].name)) {
				document.getElementById("err_"+form.elements[i].name).innerHTML = "";
			}
			document.getElementById(form.elements[i].name).style.borderColor = "#E2E2CD";	
		}	
	}
	
	return returnValue;
}

