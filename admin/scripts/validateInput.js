
function validateInput(form) 
{
	var returnValue = true;
	
	for(var i=0; i<form.length; i++) {
		
		if(form.elements[i].value.trim() == "") { 
		
			document.getElementById(form.elements[i].name).style.borderColor = "#F58C8D";
			document.getElementById(form.elements[i].name).style.borderWidth = "2px";
			
			if(document.getElementById("err_"+form.elements[i].name)) {
				
				if(document.getElementById("err_"+form.elements[i].name+"_msg"))
					document.getElementById("err_"+form.elements[i].name).innerHTML = 
					document.getElementById("err_"+form.elements[i].name+"_msg").value;
				else
				document.getElementById("err_"+form.elements[i].name).innerHTML = "Field Missing !";
			}
			
			returnValue = false;
			
		}
		else { 
			
			if(document.getElementById("err_"+form.elements[i].name)) {
				document.getElementById("err_"+form.elements[i].name).innerHTML = "";
			}
			document.getElementById(form.elements[i].name).style.borderColor = "#E2E2CD";	
		}	
	}
	return returnValue;
}

