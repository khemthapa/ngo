<?php
include_once("../db/DatabaseConnection.php");
include_once("../implements/Log.php");
include_once("../common/functions.php");
include_once("../common/Session.php");

if(isset($_POST["loginButton"]))
{

	$nameInput = input($_POST["userName"]);
	
	$passwordInput = input($_POST["userPassword"]);
	
	$dbConnect = DatabaseConnection::getDbInstance();
	
	$loggedUser = new Log();
	
	$loginText = $loggedUser->validUser($nameInput, $passwordInput);
	
	if($loginText == true) {
	
		Session::setSessionLogin("ValidAdmin", $nameInput);
		
		header("Location: dashboard.php");
		
	}
	else {
	
		$loginText = "Entered Username/Password is invalid";
		
	}
}
	
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Admin Page</title>
		<!-- <meta name="viewport" content="width=device-width"> -->
		<link href="css/main.css" media="all" type="text/css" rel="stylesheet" />
		<script src="scripts/validateInput.js"></script>
	</head>
	
	<body onload="document.loginForm.userName.focus();">
		<p class="header_text">The Well Being Foundation</p>
		<div id="small_login">
		
		<div id = "container">
		Lets go to Dashboard
		<?php
		if(isset($loginText))
		echo "<br /> <span class='missingError'>".$loginText."</span>";
		?>
			<form id="loginForm" method="post" name="loginForm" onSubmit="return validateInput(this)">
				
					<p> Username: <br />
						<input id="userName" class="input_text" type="text" name="userName" />
						<span id="err_userName" class="missingError"></span>
					</P>
					<p> Password: <br />
						<input id="userPassword" class="input_text" type="password" name="userPassword" />
						<span id="err_userPassword" class="missingError"></span>
					</P>
					<input id="loginButton" name="loginButton" class="log_button" type="submit" value="Login" />
					<input id="loginButton" name="loginButton" type="hidden" value="loginNgo" />
			</form>
		
		</div>

		</div>

	</body>
	
</html>