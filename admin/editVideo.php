<?php

include_once '../implements/Select.php';
include_once '../implements/Update.php';
$request = new RequestProtection();

if(isset($_GET['id'])) 
	$vidId = $_GET['id'];
else
	header("Location: error403.php");
	
$dbConnect = DatabaseConnection::getDbInstance();

$selEvent= new Select();
$resEventData = $selEvent->selectRecs("tbl_event", array('id', 'eventTitle'));

$resSelect = new Select();
$resData = $resSelect->selectRec("tbl_video", "*", "WHERE id='$vidId'");

if(mysql_num_rows($resData) == 1)
	$rowData = mysql_fetch_object($resData);
else
	header("Location: error403.php");
	
if(isset($_POST['editButton']) && $request->is_valid())
{
	$videoTitle = $_POST['videoTitle'];
	$videoLink = $_POST['videoLink'];
	$eventId = $_POST['eventId'];
	
	$editVideo= new Update();
	$resAdd = $editVideo->editContents('tbl_video', array('videoTitle' => $videoTitle,
											'videoLink' => $videoLink,
											'eventId' => $eventId
											),
										"WHERE id='$vidId'");
	if($resAdd)
	{
		header('Location: dashboard.php?page=manageVideos');
	}
		
}
?>

<div id="welcome_page">	

	<span class="content_header"> Edit Video</span>
	
	<form id="addImagesForm" method="post" name="addImagesForm"  enctype="multipart/form-data" onSubmit="return validateInputImageForm(this)">
	
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />
	
	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">

	<p id="err_eventTitle" class="notifyError"></p> 

	<tr>
		<td height="45" width="0" >Event Title: *</td>
		
		<td height="45" width="0">

			 <select id = "eventId" class="form_text" name="eventId" style="padding:0px;">
               <option value = "-1">Select an Event</option>
			   <?php
			
			   	if(mysql_num_rows($resEventData) == 0)
				{ 
				?>
					<option value = "-1">No Events</option>
				<?php
				}
				else
				{ 
					while($rowEventData = mysql_fetch_object($resEventData)) {
				   ?>
					   <option value = "<?php echo $rowEventData->id?>"
					   <?php echo ($rowData->eventId == $rowEventData->id)?"selected":"";?> >
					   <?php echo $rowEventData->eventTitle;?>
					   </option>
				   <?php
					}
				}
			   ?>
             </select>
			
		</td>
		
	</tr>
	
	<tr>
		<td height="45" width="0" >Video Title:</td>
		
		<td height="45" width="0">
		
			<input id="videoTitle" class="form_text" type="text" name="videoTitle" value="<?php echo $rowData->videoTitle?>"  />
			
		</td>
		
	</tr>

	<tr>
		<td height="5" width="0" colspan="2"></td>
	</tr>	
	
	<tr>
	
		<td height="45" width="0">Embedded Video Link: </td>
		
		<td height="45" width="0" >
			<input id="videoLink" class="video_text" type="text" name="videoLink"  value="<?php echo $rowData->videoLink?>"/>
		</td>
		
	</tr>
	
	
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="editButton" name="editButton" class="log_button" type="submit" value="Edit" />
		<input id="editButton" name="editButton" type="hidden" value="editVideos" />
		</td>
	</tr>
	</table>
	
	</form>
</div>
