<?php
class PageFunction
{
	function __Construct($pageName)
	{
		switch($pageName) 
		{
							
			case "managePages":
				  include($pageName.".php");
				  break;
				 
			case "manageImages":
				  include($pageName.".php");
				  break;
				  
			case "manageVideos":
				  include($pageName.".php");
				  break;
				  
			case "manageEvents":
				  include($pageName.".php");
				  break;
			
			case "home":
				  include($pageName.".php");
				  break;
				  
			case "changePassword":
				  include($pageName.".php");
				  break;
			
			case "editPage":
				  include($pageName.".php");
				  break;
			
			case "addEvent":
				  include($pageName.".php");
				  break;
			
			case "addImages":
				  include($pageName.".php");
				  break;
			
			case "manageCourses":
				  include($pageName.".php");
				  break;
				  
			case "addCourse":
				  include($pageName.".php");
				  break;
			
			case "addVideos":
				  include($pageName.".php");
				  break;
				  
			case "editCourse":
				  include($pageName.".php");
				  break;
			
			case "editEvent":
				  include($pageName.".php");
				  break;
				  
			case "editVideo":
				  include($pageName.".php");
				  break;
			
			case "editImage":
				  include($pageName.".php");
				  break;
				  
			default:
				  include("error404.php");
			  
		}
	}
}