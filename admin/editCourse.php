<?php

include_once("../implements/Select.php");
include_once("../implements/Update.php");

$request = new RequestProtection();

if(isset($_GET['id'])) 
	$courseId = $_GET['id'];
else
	header("Location: error403.php");

$dbConnect = DatabaseConnection::getDbInstance();

$resSelect = new Select();
$resData = $resSelect->selectRec("tbl_course", "*", "WHERE id='$courseId'");

if(mysql_num_rows($resData) == 1)
	$rowData = mysql_fetch_object($resData);
else
	header("Location: error403.php");
	
if(isset($_POST['editButton']) && $request->is_valid())
{
	$courseDescrition = input($_POST["courseDescrition"]);
	
	$courseTitle = input($_POST["courseTitle"]);
	
	$editPage = new Update();
	
	$resData = $editPage->editContents("tbl_course", 
									array("courseDescription" => $courseDescrition,
									"courseTitle" => $courseTitle), 
			   "WHERE id='$courseId'");
			   
	if($resData) {
		
		Session::setSessionLogin("editedSuccessfully", $rowData->courseTitle." has been edited successfully.");
		header("Location: dashboard.php?page=manageCourses");
		
	}
		
}

?>

<div id="welcome_page">	
	<span class="content_header"> Edit Course </span>
	
	<form id="editPageForm" method="post" name="editPageForm" onSubmit="return validateInput(this)">

	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />

	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">
	
	<tr>
		<td height="45" width="0" >Course Title:</td>
		
		<td height="45" width="0">
		
			<input id="courseTitle" class="form_text" type="text" name="courseTitle" value="<?php echo $rowData->courseTitle?>" />
			
		</td>
		
	</tr>
	
	<tr>
		<td height="45" width="0" valign="top">Course Description: *</td>
		
		<td height="45" width="0" > 
		
			<textarea name="courseDescrition" id="courseDescrition" rows="4" class="form_textarea" ><?php echo $rowData->courseDescription;?></textarea>
			<br/><span id="err_courseDescrition" class="missingError"></span> 
			
		</td>
		
	</tr>
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="editButton" name="editButton" class="log_button" type="submit" value="Edit" />
		<input id="editButton" name="editButton" type="hidden" value="editContent" />
		</td>
	</tr>
	</table>
	
	</form>
</div>
