<?php

include_once("../implements/Select.php");

$dbConnect = DatabaseConnection::getDbInstance();

$resSelect = new Select();
$resData = $resSelect->selectRecs("tbl_video", "*");
$count = 0;

?>

<div id="welcome_page">  
	<span class="content_header" > 
	Manage Videos | <a href="dashboard.php?page=addVideos" class="newAction">Add New Video</a> 
	</span>
	<table id="page_table" name="page_table" cellpadding="0" cellspacing="0" width="100%">
	<?php 
	
	if(isset($_SESSION['editedSuccessfully'])) 
	{
		echo "<p class='notifySuccess'>".$_SESSION['editedSuccessfully']."</p>";
		unset($_SESSION['editedSuccessfully']);
	}
	if(isset($_SESSION['addedSuccessfully'])) 
	{
		echo "<p class='notifySuccess'>".$_SESSION['addedSuccessfully']."</p>";
		unset($_SESSION['addedSuccessfully']);
	}
	if(isset($_SESSION['deleteSuccessFully'])) 
	{
		echo "<p class='notifySuccess'>".$_SESSION['deleteSuccessFully']."</p>";
		unset($_SESSION['deleteSuccessFully']);
	}
	
	if(mysql_num_rows($resData) == 0)
	{
		echo "<tr><td colspan='4' class='noRecsFound'>No Records Found</td></tr>";
	}
	else
	{
	?>
	<tr>
		<th height="30" width="8%" align="left">S.N.</th>
		<th height="30" width="0" align="left">Video Title</th>
		<th height="30" width="0" align="left">Event</th>
		<th height="30" width="0" align="left">Added Date</th>
		<th height="30" width="0" align="left">Actions</th>
	</tr>
	<?php
			
			while($rowData = mysql_fetch_object($resData)):
			$count++;
	?>
	<tr>
		<td height="30" width="8%"><?php echo $count;?></td>
	    <td height="30" width="0"><?php echo $rowData->videoTitle;?></td>
		<td height="30" width="0" align="left"> <?php echo getTitle('tbl_event', "eventTitle_".$rowData->eventId)?></td>
		<td height="30" width="0"><?php echo date("Y-m-d");?></td>
		<td height="30" width="0"> 
		<form id="delForm_<?php echo $rowData->id?>" name="delForm_<?php echo $rowData->id?>" action="delete.php" method="POST">
		<a href="dashboard.php?page=editVideo&id=<?php echo $rowData->id ?>" class="newAction">Edit</a> |
		<a href="javascript:document.getElementById('delForm_<?php echo $rowData->id?>').submit()" class="newAction">Delete</a>
		<input type="hidden" id="video_<?php echo $rowData->id?>" name="video_<?php echo $rowData->id?>" />
		</form>
		</td>
	<tr>
	<?php 
			endwhile;
			
		} //end of else
	?>
	</table>
						
</div>