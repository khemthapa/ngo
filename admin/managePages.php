<?php

include_once("../implements/Select.php");

$dbConnect = DatabaseConnection::getDbInstance();

$resSelect = new Select();
$resData = $resSelect->selectRecs("tbl_page", "*");
$count = 0;

?>

<div id="welcome_page">  

	<span class="content_header" > Manage Pages </span>
	<?php
	if(isset($_SESSION['editedSuccessfully'])) 
		{
			echo "<p class='notifySuccess'>".$_SESSION['editedSuccessfully']."</p>";
			unset($_SESSION['editedSuccessfully']);
		}
	?>
	<table id="page_table" name="page_table" cellpadding="0" cellspacing="0" width="100%">
	
	<?php 
		if(mysql_num_rows($resData) == 0)
		{
			echo "<tr><td colspan='4'>No Records Found</td></tr>";
		}
		else
		{
	?>
	<tr>
		<th height="30" width="8%" align="left" >S.N.</th>
		<th height="30" width="0" align="left" >Page Title</th>
		<th height="30" width="0" align="left" >Last Updated</th>
		<th height="30" width="0" align="left" >Actions</th>
	</tr>
	<?php
			while($rowData = mysql_fetch_object($resData)):
			$count++;
	?>
			<tr>
				<td height="30" width="8%" align="left"> <?php echo $count.".";?> </td>
				<td height="30" width="0" align="left"> <?php echo $rowData->pageName;?> </td>
				<td height="30" width="0" align="left"> <?php echo $rowData->pageUpdated;?> </td>
				<td height="30" width="0" align="left"> <a href="dashboard.php?page=editPage&id=<?php echo $rowData->id;?>"  class="newAction">Edit</a> </td>
			</tr>
	<?php 
			endwhile;
			
		}// end of else
	?>
	</table>

</div>