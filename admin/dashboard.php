<?php

include_once '../db/DatabaseConnection.php';
include_once '../implements/Log.php"';
include_once '../common/functions.php';
include_once '../common/Session.php';
include_once 'factory/PageFunction.php';
include_once '../common/RequestProtection.php';
Session::checkSession();
//$request = new RequestProtection();

?>

<!DOCTYPE HTML>
<html>

	<head>
	<meta charset="UTF-8" />
	<?php //echo $request->meta_tag(); ?>
	<title>The Well Being Foundation</title>
	<link href="css/admin.css" media="all" type="text/css" rel="stylesheet" />
	<link href="css/main.css" media="all" type="text/css" rel="stylesheet" />
	<link href="css/content.css" media="all" type="text/css" rel="stylesheet" />
	<!--<script src="scripts/ajax.js"></script>-->
	<script src="scripts/validateInput.js"></script>
	<script src="scripts/validateInputImageForm.js"></script>
	<script src="scripts/addMore.js"></script>
	<script src="scripts/addMoreVideos.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="jsDatePicker/jsDatePick_ltr.min.css" />
	<script type="text/javascript" src="jsDatePicker/jsDatePick.min.1.3.js"></script>
	<script>
		var d = new Date();
		if((d.getMonth()+1) == 13) { selMonth = 1;} else { selMonth = d.getMonth()+1; }
		window.onload = function(){
				new JsDatePick({
					useMode:2,
					isStripped:true,
					target:"eventDate",
					dateFormat:"%d-%m-%Y",
					selectedDate:{				
						day:d.getDate(),						
						month:selMonth,
						year:d.getFullYear()
					},
				});
			};
	</script>
	</head>

	<body>
	
		<div id="banner">
			<p class="header_text">The Well Being Foundation</p>
		</div>
		
		<div id="menu_container">
		
			<div id="menu_bar">
			
			 <ul>Manage Contents
			 
				 <li> - <a href="dashboard.php?page=managePages">Pages</a> </li>
				 <li> - <a href="dashboard.php?page=manageImages">Images</a></li>
				 <li> - <a href="dashboard.php?page=manageVideos">Videos</a> </li>
				 <li> - <a href="dashboard.php?page=manageEvents">Events</a> </li>
				 <li> - <a href="dashboard.php?page=manageCourses">Courses</a> </li>
			 </ul>
							 
			</div>
						
				<div id="content" >
					<?php 
					if(isset($_GET['page']))
					{
						$pageName = $_GET['page'];
						new PageFunction($pageName);
					}
					else
					{
						include("home.php");
					}
					?>
				</div>
				
			<div id="menu_bar" >
			 
			 <div id="menuText">  
				<a href="dashboard.php?page=home" id="changePassword" name="changePassword">Home</a> 
			 </div>
			 
			 <div id="menuText">  
				<a href="dashboard.php?page=changePassword" id="changePassword" name="changePassword">Change Password</a> 
			 </div>
			 
			 <div id="menuText"> 
			 <a href="logout.php" id="logOut" name="logOut">Logout</a> 
			 </div>
			 
			</div>


		</div>
		<!-- <div id="footer">
			ALL Rights Reserved @ The Well Being Foundation <?php echo date("Y")?>
		</div> -->
	</body>

</html>