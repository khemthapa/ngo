<?php

include_once '../db/DatabaseConnection.php';
include_once '../common/Session.php';
include_once '../common/functions.php';
include_once '../implements/Select.php';

if(isset($_POST))
{	
	$dbConnect = DatabaseConnection::getDbInstance();
	
	foreach($_POST as $key=>$val)
	{
		$expKey  = explode("_",$key);
		if(sizeof($expKey) == 2)
		{
			$tableName = "tbl_".$expKey[0];
			$rowId = $expKey[1];
		}
		
		if($tableName == "tbl_image")
		{
			$imageId = $rowId;
			$imageName = getTitle('tbl_image', "imageName_".$imageId);
			unlink('../well_images/'.$imageName);
		}
		if($tableName == "tbl_event")
		{
			$eventId = $rowId;
			$selImage = new Select();
			$resSelImage = $selImage->selectRec('tbl_image', array('imageName'), "WHERE eventId='$eventId'");
			if(mysql_num_rows($resSelImage) > 0)
			{
				while($row = mysql_fetch_object($resSelImage))
				{
					$imageName = $row->imageName;
					unlink('../well_images/'.$imageName);
				}
				
			}
			
			$delImage= "DELETE FROM tbl_image WHERE eventId='$rowId'";
			$rsDelImage = mysql_query($delImage);
			
			$delVideo = "DELETE FROM tbl_video WHERE eventId='$rowId'";
			$rsDelVideo = mysql_query($delVideo);
					
		}
		
		$delSql = "DELETE FROM $tableName WHERE id='$rowId'";
		$rsDel = mysql_query($delSql);
		if($rsDel)
		{
			$redirectLink = "manage".ucfirst($expKey[0])."s";
			header("Location: dashboard.php?page=$redirectLink");
			Session::setSessionLogin("deleteSuccessFully", "Deleted successfully.");
		}
	}
	
}
else
{
	header("Location: error404.php");
}

?>