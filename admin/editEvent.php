<?php

include_once '../implements/Select.php';
include_once '../implements/Update.php';
$request = new RequestProtection();

if(isset($_GET['id'])) 
	$eventId = $_GET['id'];
else
	header("Location: error403.php");

$dbConnect = DatabaseConnection::getDbInstance();

$resSelect = new Select();
$resData = $resSelect->selectRec("tbl_event", "*", "WHERE id='$eventId'");

if(mysql_num_rows($resData) == 1)
	$rowData = mysql_fetch_object($resData);
else
	header("Location: error403.php");

if(isset($_POST['editButton']) && $request->is_valid())
{
	$eventDescription = input($_POST["eventDescription"]);
	
	$eventTitle = input($_POST["eventTitle"]);
	
	$eventDate = input($_POST["eventDate"]);
	
	$eventStartTime = input($_POST["eventStartTime"]);
	
	$eventEndTime = input($_POST["eventEndTime"]);
	
	$editPage = new Update();
	
	$resData = $editPage->editContents("tbl_event", 
									array("eventDescription" => $eventDescription,
									"eventTitle" => $eventTitle,
									"eventDate" => $eventDate,
									"eventStartTime" => $eventStartTime,
									"eventEndTime" => $eventEndTime
									), 
			   "WHERE id='$eventId'");
			   
	if($resData) {
		
		Session::setSessionLogin("editedSuccessfully", $rowData->eventTitle." has been edited successfully.");
		header("Location: dashboard.php?page=manageEvents");
		
	}
		
}

?>

<div id="welcome_page">	
	<span class="content_header"> Edit Course </span>
	
	<form id="editPageForm" method="post" name="editPageForm" onSubmit="return validateInput(this)">
		
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />

	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">
	
	<tr>
		<td height="45" width="0" >Event Title:</td>
		
		<td height="45" width="0">
		
			<input id="eventTitle" class="form_text" type="text" name="eventTitle" value="<?php echo $rowData->eventTitle?>" />
			
		</td>
		
	</tr>
	<tr>
		<td height="45" width="0" >Event Date:</td>
		
		<td height="45" width="0">
		
			<input id="eventDate" class="form_text" type="text" name="eventDate" value="<?php echo $rowData->eventDate?>" />

		</td>
		
	</tr>

	<tr>
		<td height="45" width="0" >Start Time:</td>
		
		<td height="45" width="0">
		<select id="eventStartTime" name="eventStartTime" class="time_text" >
			<?php for($d=00; $d<24; $d++):
				if($d<10)  $startDate = '0'.$d;
				else $startDate = $d;
			?>
			<option value="<?php echo $startDate;?>" <?php echo(($startDate."00")==($rowData->eventStartTime."00"))?"selected":""; ?> >
			<?php echo $startDate.'00';?></option>
			<?php endfor;?>
		</select> HRS
		</td>
		
	</tr>
	<tr>
	<td height="45" width="0" >End Time:</td>
	<td height="45" width="0">
			<select id="eventEndTime" name="eventEndTime" class="time_text" >
				<?php for($d=00; $d<24; $d++): 
					if($d<10)  $endDate = '0'.$d;
					else $endDate = $d;
				?>
				<option value="<?php echo $endDate;?>" <?php echo(($endDate."00")==($rowData->eventEndTime."00"))?"selected":""; ?>> 
				<?php echo $endDate.'00'?>
				</option>
				<?php endfor;?>
			</select> HRS
			</td>
	</tr>
	
	<tr>
		<td height="45" width="0" valign="top">Event Description: *</td>
		
		<td height="45" width="0" >
		
			<textarea name="eventDescription" id="eventDescription" rows="4" class="form_textarea" ><?php echo $rowData->eventDescription;?></textarea>
			
		</td>
		
	</tr>
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="editButton" name="editButton" class="log_button" type="submit" value="Edit" />
		<input id="editButton" name="editButton" type="hidden" value="editContent" />
		</td>
	</tr>
	</table>
	
	</form>
</div>
