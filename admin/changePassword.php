<?php

include_once("../implements/Select.php");
include_once("../implements/Update.php");

$request = new RequestProtection();
$dbConnect = DatabaseConnection::getDbInstance();

$resSelect = new Select();
$resData = $resSelect->selectRecs("tbl_admin", "*");

if(mysql_num_rows($resData) == 1)
	$rowData = mysql_fetch_object($resData);
else
	header("Location: error403.php");
	
if(isset($_POST['changeButton']) && $request->is_valid())
{
	
	$proceed = true;
	
	$adminPassword = input($_POST["adminPassword"]);
	
	$confirmPassword = input($_POST["confirmPassword"]);
	
	if($rowData->adminPassword != md5($_POST["oldPassword"]))
	{
		Session::setSessionLogin("matchError", "Old Password didnot match.");
		$proceed = false;
	}
	if($adminPassword != $confirmPassword )
	{
		Session::setSessionLogin("confirmError", "Confirm Password didnot match");
		$proceed = false;
	}
	
	if($proceed)
	{
		$editPage = new Update();
		$resData = $editPage->editContents("tbl_admin", 
					array( "adminPassword" => md5($adminPassword) ), 
				   "WHERE 1");
				   
		if($resData)
			Session::setSessionLogin("editedSuccessfully", "Password has been changed successfully.");

	}
		
}

?>

<div id="welcome_page">  

	<span class="content_header" > Change Password </span>
	
	<form id="changePasswordForm" method="post" name="changePasswordForm" 
	onSubmit="return validateInput(this);">
	
	<!-- csrf token-->
	<input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $request->hash;?>" />

	<table id="content_table" name="content_table" cellpadding="0" cellspacing="0" width="100%">
	
	<?php
	if(isset($_SESSION['editedSuccessfully'])) 
	{
		echo "<p class='notifySuccess'>".$_SESSION['editedSuccessfully']."</p>";
		unset($_SESSION['editedSuccessfully']);
	}
	if(isset($_SESSION['matchError'])) 
	{
		echo "<p class='notifyError'>".$_SESSION['matchError']."</p>";
		unset($_SESSION['matchError']);
	}
	if(isset($_SESSION['confirmError'])) 
	{
		echo "<p class='notifyError'>".$_SESSION['confirmError']."</p>";
		unset($_SESSION['confirmError']);
	}

	?>
	
	<tr>
		<td height="45" width="0" >Old Password: *</td>
		
		<td height="45" width="0" >
		
			<input id="oldPassword" class="form_text" type="password" name="oldPassword" />
			
		</td>
		
	</tr>
	<tr>
		<td height="45" width="0" >New Password: *</td>
		
		<td height="45" width="0" >
		
			<input id="adminPassword" class="form_text" type="password" name="adminPassword" />
			
		</td>
		
	</tr>

	<tr>
		<td height="45" width="0" >Confirm Password: *</td>
		
		<td height="45" width="0" >
		
			<input id="confirmPassword" class="form_text" type="password" name="confirmPassword" /> 
			
		</td>
		
	</tr>
 
	<tr>
		<td height="45">&nbsp;</td>
		<td height="45">
		<input id="changeButton" name="changeButton" class="log_button" type="submit" value="Change" />
		<input id="changeButton" name="changeButton" type="hidden" value="changePassword" />
		</td>
	</tr>
	</table>
	
	</form>
	
						
</div>