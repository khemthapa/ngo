<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/front.css">
<script type="text/javascript" src="js/slideshow.js"></script>
<script type="text/javascript" src="js/facebookLike.js"></script>
<title>The Wellbeing foundation - Gallery</title>
</head>

<body>
<?php

  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Select.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/db/DatabaseConnection.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Event.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Image.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Video.php';

  $imagePath = "well_images/";
  $dbConnect = DatabaseConnection::getDbInstance();

  $resSelect = new Select();
  $resData = $resSelect->selectRecs("tbl_event", "*");

  if(mysql_num_rows($resData) > 0)   $hasEvent = true;
  else   $hasEvent = false;

?>

<div class="container">

	<?php include 'header.php'; ?>

  <div class="sidebar1">

    <?php include 'sidebar.php' ?>
      
  </div>  <!-- end .sidebar1 -->
  <div class="content">
  	 <div id="slideshow">
	  <img src="images/gambia 9.jpg" alt="Home" width="100%" height="260" class="active"/>
	  <img src="images/Gambia Chloe 1.jpg" alt="Home" width="100%" height="260"/>
	  <img src="images/Gambia Chloe 17.jpg" alt="Home" width="100%" height="260"/>
	  <img src="images/Gambia Dan 54.jpg" alt="Home" width="100%" height="260"/>
	  </div>
    <h2><a href='gallery?type=images' style='text-decoration:none;'>Images</a> | 
        <a href='gallery?type=videos' style='text-decoration:none;'>Videos</a></h2>
    <hr />
   
    <?php 

    $eventImage = new Image();
    $eventVideo= new Video();
    if($hasEvent):
        
      if(isset($_GET['type']) && $_GET['type'] == 'images'):
          
           while($rowData = mysql_fetch_object($resData))
           {
             echo "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-bottom: 1px solid #CFCFCF'>
                    <tr><td><h5> $rowData->eventTitle</h5> </td></tr><tr>" ;
              if($eImages = $eventImage->get_event_images($rowData->id, 'all'))
              {
                $countImages = 1;
                foreach($eImages as $img) 
                { 
                   $imagePathName = $imagePath.$img['imageName'];
                   $imageTitle = $img['imageTitle'];
                   echo "<td width='0' style='padding:10px;'>";
                   if(file_exists($imagePathName))
                   {
                     echo "<img src='$imagePathName' width='175' height='175' title='$imageTitle' style='float:left'/>";
                   }
                   else
                     echo "<img src='' width='175' height='175' title='Image Not Available'/>";
                   echo "</td>";
                   if($countImages == 4) echo "</tr><tr>";
                   $countImages++;
                }
              }

             echo "</tr></table>";
          }

      elseif (isset($_GET['type']) && $_GET['type'] == 'videos'):

           while($rowData = mysql_fetch_object($resData))
           {
             echo "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-bottom: 1px solid #CFCFCF'>
                    <tr><td><h5> $rowData->eventTitle</h5> </td></tr><tr>" ;
              if($eImages = $eventVideo->get_event_videos($rowData->id, 'all'))
              {
                $countVideos= 1;
                foreach($eImages as $img) 
                { 
                   $videoTitle = $img['videoTitle'];
                   $videoLink = $img['videoLink'];
                   echo "<td width='0' style='padding:10px;'>".$videoLink. "</td>"; 
                   if($countVideos == 2) echo "</tr><tr>";
                   $countVideos++;
                }
              }

             echo "</tr></table>";
          }
      else:

         while($rowData = mysql_fetch_object($resData))
           {
             echo "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-bottom: 1px solid #CFCFCF'>
                    <tr><td><h5> $rowData->eventTitle</h5> </td></tr><tr>" ;
              if($eImages = $eventImage->get_event_images($rowData->id, 'all'))
              {
                $countImages = 1;
                foreach($eImages as $img) 
                { 
                   $imagePathName = $imagePath.$img['imageName'];
                   $imageTitle = $img['imageTitle'];
                   echo "<td width='0' style='padding:10px;'>";
                   if(file_exists($imagePathName))
                   {
                     echo "<img src='$imagePathName' width='175' height='175' title='$imageTitle' style='float:left'/>";
                   }
                   else
                     echo "<img src='' width='175' height='175' title='Image Not Available'/>";
                   echo "</td>";
                   if($countImages == 2) echo "</tr><tr>";
                   $countImages++;
                }
              }

             echo "</tr></table>";
          }
      endif;

    else:
    echo "<p>No Gallery Available.</p>";
    endif; // end of hasEvent

    ?>
   </table>

    
    </div><!-- end of content -->
</div>

<div>
<?php include 'footer.php';?>
</div>
</body>
</html>