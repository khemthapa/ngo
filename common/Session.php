<?php

class Session {
	
	private static $sessionInstance = null;
	
	private function  __construct() {
	
		@session_start();

	}
	
	private static function getSessionInstance() {
		
		if(self::$sessionInstance == null)
			self::$sessionInstance = new Session();
		else
			return self::$sessionInstance;
		
	}
	
	public static function setSessionLogin($sessionName, $sessionValue) {
	
		self::getSessionInstance();

		$_SESSION[$sessionName] = $sessionValue;
		
	}
	
	public static function getSessionVariable() {
	
		self::getSessionInstance();
		
		return $_SESSION;
		
	}
	
	public static function checkSession() {

		if(!self::getSessionVariable()) {
			header("Location: index.php");
		}

	}

	public static function unsetSession($sessionName) {
	
		self::getSessionInstance();
		unset($_SESSION[$sessionName]);
		
	}
}

?>