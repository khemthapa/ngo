<?php

function input($userInput)
{
	$validInput = trim($userInput);
	
	$validInput = mysql_real_escape_string($validInput);
	
	return $validInput;
}

function getExtension($fileName)
{
	$expPeriod = explode(".", $fileName);
	
	$totalSize = sizeof($expPeriod);
	
	return strtolower($expPeriod[$totalSize-1]);
	
}

function getRandomKeys($length)
{
	$characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $randomString;
}

function getTitle($tableName, $titleRowId) 
{
	$title = "Not Found";
	$expTitleId = explode("_",$titleRowId);
	$find = $expTitleId[0]; 
	$rowId = $expTitleId[1];
	$resSelect = new Select();
	$resSelData = $resSelect->selectRec($tableName, array($find), "WHERE id='$rowId' LIMIT 1");
	if(mysql_num_rows($resSelData) > 0)
	{
		$row = mysql_fetch_object($resSelData);
		$title = $row->$find;
	}
	
	return $title;
	
}