<?php
 class RequestProtection {

 	public $hash;
 	private $previous_hash;

 	public function __construct()
 	{
 		if(isset($_SESSION['request_token']))
 			$this->previous_hash = $_SESSION['request_token'];

 		$this->hash = $_SESSION['request_token'] = md5(uniqid());
 	}

 	public function meta_tag($key = 'csrf_token')
 	{
 		return "<meta name='$key' value='$this->hash' />";
 	}

 	public function is_valid($key = 'csrf_token')
 	{
 		return isset($_POST[$key]) && $_POST[$key] === $this->previous_hash;
 	}

 }