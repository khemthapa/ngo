<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/front.css">
<script type="text/javascript" src="js/slideshow.js"></script>
<script type="text/javascript" src="js/facebookLike.js"></script>
<title>The Wellbeing Foundation - Events</title>
</head>

<body>

  <?php

  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Select.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/db/DatabaseConnection.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Event.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/implements/Image.php';

  $imagePath = "well_images/";
  $dbConnect = DatabaseConnection::getDbInstance();

  $resSelect = new Select();
  $resData = $resSelect->selectRecs("tbl_event", "*");

  if(mysql_num_rows($resData) > 0)   $hasEvent = true;
  else   $hasEvent = false;

  ?>

<div class="container">

  <?php include 'header.php'; ?>

  <div class="sidebar1">

     <?php include 'sidebar.php'; ?>

  </div>     <!-- end .sidebar1 -->

  <div class="content">
    <div id="slideshow">
    <img src="images/gambia 9.jpg" alt="Home" width="100%" height="260" class="active"/>
    <img src="images/Gambia Chloe 1.jpg" alt="Home" width="100%" height="260"/>
    <img src="images/Gambia Chloe 17.jpg" alt="Home" width="100%" height="260"/>
    <img src="images/Gambia Dan 54.jpg" alt="Home" width="100%" height="260"/>
    </div>

    <?php 

      if($hasEvent):
      
        $eventObj = new Event($resData);

        $upcomingEvents = $eventObj->get_upcoming_events();
        $pastEvents = $eventObj->get_past_events();

        $eventImage = new Image();

        if(sizeof($upcomingEvents) > 0)
        {
            echo "<h2>Upcoming Events</h2><hr >";

            foreach($upcomingEvents as $ue) {

            echo "<div style='padding:10px; '><p>";

             if($eImages = $eventImage->get_event_images($ue['id'], '1'))
             {
                foreach($eImages as $img) 
                { 

                  $imagePathName = $imagePath.$img['imageName'];
                  $imageTitle = $img['imageTitle'];

                  if(file_exists($imagePathName))
                  {
                    echo "<img src='$imagePathName' width='125' height='125' title='$imageTitle' style='float:left'/>";
                  }
                  else
                    echo "<img src='' width='125' height='125' title='Image Not Available'/>";
                }
              }

              echo  "<div style='position:relative; top:-15px; left:0px;'><h3>&nbsp;".$ue['eventTitle']."</h3>"."<p>&nbsp;".$ue['eventDescription'].
              "</p>"."<p>&nbsp;Event Date: ".$ue['eventDate']."</p></div></p></div>";
              echo "<br/><hr/>";

              }

        }
       
       if(sizeof($pastEvents) > 0)
        {
            echo "<div style='clear:both; padding-top:10px;'><h2>Past Events</h2>";
            echo "<hr>";
            echo "<p>";
            foreach($pastEvents as $pe) {
              echo "<div style='padding:10px; '><p>";

             if($eImages = $eventImage->get_event_images($pe['id'], '1'))
             {
                foreach($eImages as $img) 
                { 

                  $imagePathName = $imagePath.$img['imageName'];
                  $imageTitle = $img['imageTitle'];

                  if(file_exists($imagePathName))
                  {
                    echo "<img src='$imagePathName' width='125' height='125' title='$imageTitle' style='float:left'/>";
                  }
                  else
                    echo "<img src='' width='125' height='125' title='Image Not Available'/>";
                }
              }

               echo  "<div style='position:relative; top:-15px; left:0px;'><h3>&nbsp;".$pe['eventTitle']."</h3>"."<p>&nbsp;".$pe['eventDescription'].
              "</p>"."<p>&nbsp;Event Date: ".$pe['eventDate']."</p></div></p></div>";
              echo "<br/><hr/>";
            }
            echo "</p></div>";
        }

      
      else
         echo "<p> Unfortunatley there are no events. please keep on visiting for later updates. </p>";

      endif;

   ?>
  </div>  <!-- end .content -->

</div>  <!-- end .container -->
    
 <div >
    <?php include 'footer.php'; ?>
  </div>
</body>
</html>